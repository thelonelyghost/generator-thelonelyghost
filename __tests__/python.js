'use strict';

const path = require(`path`);
const assert = require(`yeoman-assert`);
const helpers = require(`yeoman-test`);

describe(`generator-thelonelyghost:python`, () => {
  beforeAll(() => {
    return helpers.run(path.join(__dirname, `../generators/python`)).withPrompts({
      projectName: `my_test_proj`,
      authorName: `John Jacob Jingleheimer Schmidt`,
      email: `jjjs@example.com`,
      gitlabUsername: `jjjsIsTheBest`,
      license: `MIT`
    });
  });

  it(`creates python project files`, () => {
    assert.file([`.gitignore`, `setup.py`, `setup.cfg`, `my_test_proj/__init__.py`]);
  });

  it(`creates a license`, () => {
    assert.file(`LICENSE`);
  });

  it(`creates documentation`, () => {
    assert.file([`README.rst`, `CONTRIBUTING.rst`]);
  });

  it(`uses flake8 for linting`, () => {
    assert.fileContent(`setup.py`, /\bflake8\b/);
    assert.fileContent(`setup.cfg`, /\[flake8\]/);
  });

  it(`uses pytest`, () => {
    assert.fileContent(`setup.py`, /\bpytest\b/);
    assert.fileContent(`setup.cfg`, /\[tool:pytest\]/);
  });

  it(`measures test coverage`, () => {
    assert.fileContent(`setup.py`, /\bpytest-cov\b/);
    assert.fileContent(`setup.cfg`, /\baddopts = .*--cov=my_test_proj\b/);
    assert.fileContent(`setup.cfg`, /\[coverage:/);
  });

  it(`sets the default version of project to v0.1.0`, () => {
    assert.fileContent(`my_test_proj/__init__.py`, /__version__ = .0\.1\.0./);
  });

  it(`creates tests`, () => {
    assert.file([`tox.ini`, `tests/__init__.py`, `tests/test_example.py`]);
    assert.fileContent(`tests/test_example.py`, /class Test\w+\(unittest\.TestCase\):/);
    assert.fileContent(`tests/test_example.py`, /def setUp\(self\):/);
  });

  it(`creates a python-oriented direnv config`, () => {
    assert.file(`.envrc`);
    assert.fileContent('.envrc', /\bpython\b/);
  });
});
