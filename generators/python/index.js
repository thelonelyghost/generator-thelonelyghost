'use strict';

const Generator = require(`yeoman-generator`);
const validators = require(`./validators`);
const path = require(`path`);

module.exports = class extends Generator {
  constructor(args, opts) {
    super(args, opts);

    this.option(`projectName`, {
      defaults: this.config.get(`projectName`),
      desc: `Name of your project.`,
      type: String
    });

    this.option(`authorName`, {
      defaults: this.config.get(`authorName`),
      desc: `Your full name`,
      type: String
    });

    this.option(`email`, {
      defaults: this.config.get(`email`),
      desc: `Your email address.`,
      type: String
    });

    this.option(`gitlabUsername`, {
      defaults: this.config.get(`gitlabUsername`),
      desc: `Your GitLab username.`,
      type: String
    });

    this.option(`ciProvider`, {
      defaults: this.config.get(`ciProvider`) || 0,
      desc: `CI Provider.`,
      type: Number
    });
  }

  initializing() {
    this.pkg = require(`../../package.json`);

    const updateNotifier = require(`update-notifier`);

    updateNotifier({ pkg: this.pkg }).notify();
  }

  prompting() {
    // Have Yeoman greet the user.
    if (!this.options[`skip-welcome-message`]) {
      this.log(require('yosay')(`Welcome to TheLonelyGhost's project generator!`));
    }

    const prompts = [
      {
        default: this.options.projectName,
        message: `Name of your project`,
        name: `projectName`,
        type: `input`,
        validate: validators.validateProjectName
      },
      {
        default: this.options.authorName,
        message: `Your full name`,
        name: `fullName`,
        type: `input`,
        validate: validators.validateFullName
      },
      {
        default: this.options.email,
        message: `Your e-mail address`,
        name: `email`,
        type: `input`,
        validate: validators.validateEmail
      },
      {
        default: this.options.gitlabUsername,
        message: `Your GitLab username`,
        name: `gitlabName`,
        type: `input`,
        validate: validators.validateGitLabName
      },
      {
        default: this.options.ciProvider,
        choices: require(`./ci-providers`),
        message: `Your CI provider`,
        name: `ciProvider`,
        type: `list`,
        validate: validators.validateCIProvider
      }
    ];

    return this.prompt(prompts).then(answers => {
      this.projectName = answers.projectName;
      this.config.set(`projectName`, this.projectName);

      this.authorName = answers.authorName;
      this.config.set(`authorName`, this.authorName);

      this.email = answers.email;
      this.config.set(`email`, this.email);

      this.gitlabUsername = answers.gitlabUsername;
      this.config.set(`gitlabUsername`, this.gitlabUsername);

      this.ciProvider = answers.ciProvider;
      this.config.set(`ciProvider`, this.ciProvider);
    });
  }

  writing() {
    this.composeWith(require.resolve(`generator-license`), {
      name: this.authorName,
      email: this.email
    });

    this.fs.copyTpl(
      this.templatePath(`setup.cfg.ejs`),
      this.destinationPath(`setup.cfg`),
      {
        projectName: this.projectName
      }
    );

    this.fs.copyTpl(this.templatePath(`setup.py.ejs`), this.destinationPath(`setup.py`), {
      projectName: this.projectName,
      fullName: this.authorName,
      email: this.email,
      homepage: `https://gitlab.com/${this.gitlabUserame}/${this.projectName}`
    });

    this.fs.copyTpl(this.templatePath(`_tox.ini.ejs`), this.destinationPath(`tox.ini`), {
      projectName: this.projectName
    });

    if (this.ciProvider === `Travis CI`) {
      this.fs.copy(this.templatePath(`travis.yml`), this.destinationPath(`.travis.yml`));
    }

    this.fs.copy(
      this.templatePath(`gitattributes`),
      this.destinationPath(`.gitattributes`)
    );

    this.fs.copy(this.templatePath(`gitignore`), this.destinationPath(`.gitignore`));

    this.fs.write(
      this.destinationPath(path.join(this.projectName, `__init__.py`)),
      `__version__ = '0.1.0'`
    );

    this.fs.copyTpl(
      this.templatePath(`CONTRIBUTING.rst.ejs`),
      this.destinationPath(`CONTRIBUTING.rst`),
      {
        gitlabName: this.gitlabUsername,
        projectName: this.projectName
      }
    );
    this.fs.copyTpl(
      this.templatePath(`README.rst.ejs`),
      this.destinationPath(`README.rst`),
      {
        ciProvider: this.ciProvider,
        fullName: this.fullName,
        gitlabName: this.gitlabName,
        projectName: this.projectName
      }
    );

    this.fs.write(this.destinationPath(path.join(`tests`, `__init__.py`)), ``);

    this.fs.copy(
      this.templatePath(`test_template.py`),
      this.destinationPath(path.join(`tests`, `test_example.py`))
    );

    this.fs.copy(this.templatePath(`envrc`), this.destinationPath(`.envrc`));

    this.config.set(`version`, this.pkg.version);
  }
};
