'use strict';

const ciProviders = require(`./ci-providers`);

module.exports = {
  validateProjectName: projectName => projectName.length > 0,
  validateFullName: fullName => fullName.length > 0,
  validateEmail: email => email.length > 0,
  validateGitLabName: gitlabName => gitlabName.length > 0,
  validateCIProvider: ciProvider =>
    ciProviders.indexOf(ciProvider) === -1 ? `Not a valid CI choice.` : true
};
