# generator-thelonelyghost [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url]
> TheLonelyGhost&#39;s Project scaffolding

## Installation

First, install [Yeoman](http://yeoman.io) and generator-thelonelyghost using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g generator-thelonelyghost
```

Then generate your new project:

```bash
yo thelonelyghost:python
```

## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

## License

BSD-3-Clause © [David Alexander](https://www.thelonelyghost.com/)


[npm-image]: https://badge.fury.io/js/generator-thelonelyghost.svg
[npm-url]: https://npmjs.org/package/generator-thelonelyghost
[travis-image]: https://travis-ci.org/TheLonelyGhost/generator-thelonelyghost.svg?branch=master
[travis-url]: https://travis-ci.org/TheLonelyGhost/generator-thelonelyghost
[daviddm-image]: https://david-dm.org/TheLonelyGhost/generator-thelonelyghost.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/TheLonelyGhost/generator-thelonelyghost
